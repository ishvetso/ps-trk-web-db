function ApplyAccessControl() {
  var LHCSerialNumber;
  LHCSerialNumber = document.getElementById("UpdateLHCSN");
  DetectorPart = document.getElementById("UpdateDetector");
  Transaction = document.getElementById("Transaction");
  StatusUpdate = document.getElementById("StatusUpdate");
  LHCSerialNumber.disabled = false;
  DetectorPart.disabled = false;
  Transaction.disabled = false;
  StatusUpdate.disabled = false;
}

function ApplyAccessControlForRegistration() {
  var LHCSerialNumber;
  RegisterPS = document.getElementById("RegisterPS");
  RegisterNewType = document.getElementById("RegisterNewType");
  RegisterNewLocation = document.getElementById("RegisterNewLocation");
  RegisterNewSublocation = document.getElementById("RegisterNewSublocation");
  RegisterPS.disabled = false;
  RegisterNewType.disabled = false;
  RegisterNewLocation.disabled = false;
  RegisterNewSublocation.disabled = false;
}
