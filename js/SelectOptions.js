function updateSublocations(sublocationsmap, allsublocations, locations, labelselectsublocation, labelselectlocation, onload=false){
 
    var selectsublocation = document.getElementById(labelselectsublocation);
    var selectlocation = document.getElementById(labelselectlocation);
    var selectedlocation = selectlocation.value;
    var options = selectsublocation.options;

    if (sublocationsmap[selectedlocation].length == 0) selectsublocation.value = null;
    var i;
    
    for (i=0; i<options.length; i++){
       if (!sublocationsmap[selectedlocation].includes(options[i].value) && options[i].label!="undefined"){
	options[i].hidden = true;
	options[i].selected = false;
	
      }
      else if (options[i].label=="undefined" && !onload) {
        options[i].selected = true;
        options[i].hidden = false;
      }
      else {
	options[i].hidden = false;
      }
    }

}



