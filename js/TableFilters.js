function ColumnFilter(columnname,columnnumber) {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById(columnname);
  filter = input.value.toUpperCase();
  if (filter=="*") filter="";
  table = document.getElementById("parsetable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[columnnumber];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }   
  }
}
function SelectFilter() {
  var pstype, location, status, filtertype, filterlocation,filterstatus, table, tr, tdType, tdLoc, tdStatus, i, txtValueType, txtLocation, txtStatus;
  pstype = document.getElementById("PSTypeSelected");
  location = document.getElementById("LocationSelected");
  sublocation = document.getElementById("SublocationSelected");
  status = document.getElementById("StatusSelected");
  
  filtertype = pstype.value.toUpperCase();
  filterlocation = location.value.toUpperCase();
  filtersublocation = sublocation.value.toUpperCase();
  filterstatus = status.value.toUpperCase();
  
  if (filterlocation=="*") filterlocation="";
  if (filtersublocation=="*") filtersublocation="";
  if (filtertype=="*") filtertype="";
  if (filterstatus=="*") filterstatus = "";
  
  table = document.getElementById("parsetable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    tdType = tr[i].getElementsByTagName("td")[3];
    tdLoc = tr[i].getElementsByTagName("td")[4];
    tdSubloc = tr[i].getElementsByTagName("td")[5];
    tdStatus = tr[i].getElementsByTagName("td")[7];
    
    if (tdType &&  tdLoc && tdStatus && tdSubloc) {
      txtValueType = tdType.innerText;
      txtLocation = tdLoc.innerText;
      txtStatus = tdStatus.innerText;
      txtSubloc = tdSubloc.innerText;
      if ((txtValueType.toUpperCase().indexOf(filtertype) > -1) && (txtLocation.toUpperCase().indexOf(filterlocation) > -1) && (txtStatus.toUpperCase().indexOf(filterstatus) > -1) && (txtSubloc.toUpperCase().indexOf(filtersublocation) > -1) ) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }   
  }
}