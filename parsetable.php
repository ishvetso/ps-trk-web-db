<head>
<script src="js/plotly-latest.min.js"></script>
<script src="js/TableFilters.js"></script>
<style>

div.containter {
   display: inline; 
  float: left;
  }
</style>
</head>
<H1 align='center'> Tracker power supplies database </H1> 
<H3><a href="php/DatabaseUtils.php"> Configuration utilities</a><H3>
<H3><a href="php/plot.php"> Rate of broken hardware (plot) </a><H3>
<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
 require 'php/PSUtils.php';
 require 'php/BuildTable.php';
 $psutil = new PSUtils();
 $locations = $psutil-> getLocations();
 $statuses = $psutil-> getStatuses();
 $pstypes = $psutil-> getPSTypes();
 
 $allsublocations = array();
 $sublocationmap = array();
 foreach($locations as $location){
  $sublocationmap[$location] = array();
  $sublocations = $psutil -> getSubLocationsForLocation($location);
  for ($i=0; $i<count($sublocations);$i++){
    array_push($allsublocations,$sublocations[$i]);
    array_push($sublocationmap[$location],$sublocations[$i]);
   }
 }
 echo "<div class='containter' id='pie-tot' ><br/>";
 echo "</div>";
foreach($pstypes as $pstype)
{
  echo "<div class='containter' id='pie-$pstype' ><br/>";
  echo  "</div>";
}
?>


<?php 
 // we calculate some statistics
$statistics = array();
$totalnumbersoftype = array();
foreach ($pstypes as $pstype){
  $statistics[$pstype] = array();
}
for ($i=0; $i< count($pstypes); $i++) {
 array_push($totalnumbersoftype, $psutil -> getNumberPSStatistics("*", $pstypes[$i]));
 for ($j=0; $j< count($statuses); $j++){
  $count = $psutil -> getNumberPSStatistics($statuses[$j], $pstypes[$i]);
  array_push($statistics[$pstypes[$i]], $count);
 }

}

///Construct the table
$data = $psutil -> getData('', '', '','', '');
BuildTable($pstypes, $locations, $allsublocations, $statuses, $data);
	
?>
<script>

  var statuses = <?php echo json_encode($statuses); ?>;
  var statistics  =  <?php echo json_encode($statistics); ?>;
  var mypstypes  =  <?php echo json_encode($pstypes); ?>;
  var totalnumbers  =  <?php echo json_encode($totalnumbersoftype); ?>;

  var j;
  var i;
  var divs = [];
  var allplots = [];
  
  
  for (i = 0; i < mypstypes.length ; i++){
   allplots[i] = {
    values: statistics[mypstypes[i]], 
    labels: statuses,
    type: 'pie',
    name: mypstypes[i],
    marker: {
    colors: ['cyan', 'green', 'orange',  'magenta', 'blue'],
  },
    textinfo: 'value'
  };
    divs[i] = document.getElementById('pie-'+mypstypes[i]);
    var layout = {
      title: mypstypes[i],
      height: 400,
      width: 400
    };
    
    Plotly.newPlot( divs[i], [allplots[i]], layout);
   
    divs[i].on('plotly_click', function(data){
    window.open("php/totalpertype.php?pstype="+data.points[0].fullData.name); 
  });
} 
  var totalplot = document.getElementById('pie-tot');
  var totnumplot = {
    values:totalnumbers, 
    labels: mypstypes,
    type: 'pie',
    textinfo: 'value'
  };
  layout = {
      title: 'all the hardware',
      height: 400,
      width: 400
    };
  console.log(totalnumbers, mypstypes);
  Plotly.newPlot( totalplot, [totnumplot],layout);
  
  totalplot.on('plotly_click', function(){
  window.open("php/totalstatistics.php"); 
});
  
  
 </script>