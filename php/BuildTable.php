<?php
function BuildTable($pstypes, $locations, $allsublocations, $statuses, $data){
echo "<table border='1px solid black' width='100%' id='parsetable'><br/>";
	echo "<tr align=left><br/>";
	echo "<th>Serial number<input type='number' id='serialnumber' onkeyup=ColumnFilter('serialnumber',0)></th><br/>";
	echo "<th>BARCODE<input type='number' id='barcode' onkeyup=ColumnFilter('barcode',1)></th><br/>";
	echo "<th>ESS Serial Number<input type='number' id='ESSSN' onkeyup=ColumnFilter('ESSSN',2)></th><br/>";
	echo "<th>Type";
	echo "<select id ='PSTypeSelected' onchange=SelectFilter()>";
	foreach($pstypes as $pstype)
	{
	  echo "<option value='$pstype'>$pstype</option><br/>";
	}
	echo "<option value='*' selected>*</option><br/>";
	echo "</select></th><br/>";
	
	echo  "<th>Location <select id ='LocationSelected' onchange=SelectFilter()>";
	foreach($locations as $location)
	{
	  echo "<option value='$location'>$location</option><br/>";
	}
	echo "<option value='*' selected>*</option><br/>";
	echo "</select></th><br/>";
	echo  "<th>Sublocation  <select id ='SublocationSelected' onchange=SelectFilter()>";
	foreach($allsublocations as $sublocation)
	{
	  echo "<option value='$sublocation'>$sublocation</option><br/>";
	}
	echo "<option value='*' selected>*</option><br/>";
	echo "</select></th><br/>";
	echo "<th>Detector related part<input type='text' id='detector' onkeyup=ColumnFilter('detector',6)></th><br/>";
	echo  "<th>Status<select id ='StatusSelected' onchange=SelectFilter()>";
	foreach($statuses as $status)
	{
	  echo "<option value='$status'>$status</option><br/>";
	}
	echo "<option value='*' selected>*</option><br/>";
	echo "</select></th><br/>";
	
	$curdir = getcwd();
	$prefix = '';
	if (preg_match("/php/", $curdir)) $prefix = "";
	else $prefix = "php/";
	for ($i=0; $i<count($data['SERIALNUMBER']);$i++)
	{

	  echo  "<tr>";
	  echo "<td><a href=".$prefix."psinfo.php?serialnumber=".$data['SERIALNUMBER'][$i]."&pstype=".$data['PSTYPES'][$i].">".$data['SERIALNUMBER'][$i]."</a></td>";
	  echo  "<td>".$data['BARCODE'][$i]."</td>";
	  echo  "<td>".$data['LHCSERIALNUMBER'][$i]."</td>";
	  echo  "<td>".$data['PSTYPES'][$i]."</td>";
	  echo  "<td>".$data['LOCATIONS'][$i]."</td>";
	  echo  "<td>".$data['SUBLOCATIONS'][$i]."</td>";
	  echo  "<td>".$data['DETECTORPART'][$i]."</td>";
	  echo  "<td>".$data['STATUSES'][$i]."</td>";
	  echo  "</tr>";
	}
	echo  "</table><br/>";
}
?>