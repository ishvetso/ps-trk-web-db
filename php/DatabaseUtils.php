<head>
<script type="text/javascript" src="../js/SelectOptions.js">
</script>
<script type="text/javascript" src="../js/ApplyAccessControl.js">
</script>
</head>

<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
 require 'PSUtils.php';
 require 'LdapUtils.php';

  $isMemberOfEgroup = isCurrentUserMemberOfEgroup("cms-trk-powersupplies-db_write");

 echo "<a href='../parsetable.php'>  Go to main page</a>";
 $psutil = new PSUtils();
 $locations = $psutil-> getLocations();
 $statuses = $psutil-> getStatuses();
 $pstypes = $psutil-> getPSTypes();

 $sublocationmap = array();
 $allsublocations = array();
 
 foreach($locations as $location){
  $sublocationmap[$location] = array();
  $sublocations = $psutil -> getSubLocationsForLocation($location);
  for ($i=0; $i<count($sublocations);$i++){
    array_push($sublocationmap[$location],$sublocations[$i]);
    array_push($allsublocations,$sublocations[$i]);
   }
 }
 
?>


<html>
 

<head>
<style>
.form-popup {
  display: block;
  height:190px;

} 

.box {
  width: 320px;
  padding: 25px;
  height: 20%;
  border: 5px solid gray;
  margin: 0; 
  float:left;
}

.bigbox {
  width: 320px;
  height: 42%;
  padding: 25px;
  border: 5px solid gray;
  margin: 0; 
  float:left;
}
.search {
float: block;
display: inline-block;


}
.fixed {
 top: 700px;

 position: fixed;
}
.body {
overflow: auto;
}
</style>
</head>



<body onload="callUpdateSelected('SelectSublocation','SelectLocation')">
<H2 align='center'> Database utilities </H2>
<!---Registration of New Power supplies--->
<div class="bigbox" id="box1">
<H3> Registration of power supplies<br/> </H3> 
<!---<button id="buttonopen1" onclick="openForm('regHW', 'box1', 'buttonopen','buttonclose')">Click here to register</button><br/>--->
<div class="form-popup" id="regHW">
<form method="post">
Serial Number: <input type="number" name="SN"><br>
Bar code: <input type="text" name="BARCODE"><br>
ESS serial number: <input type="text" name="ESS"><br>
Power supply type: <select name = "PSType">
<?php
foreach($pstypes as $pstype)
{
  echo "<option value='$pstype'>$pstype</option><br/>";
}
echo "</select><br/>";
echo "Location: <select name = 'Location' id='SelectLocation' onchange=callUpdateSelected('SelectSublocation','SelectLocation')><br/>";
foreach($locations as $location)
{
  echo "<option value='$location'>$location</option><br/>";
}
echo "</select><br/>";

echo "Sublocation: <select name = 'Sublocation' id='SelectSublocation' ><br/>";
foreach($allsublocations as $sublocation)
{
  echo "<option value='$sublocation'>$sublocation</option><br/>";

}
 echo "<option value='' label='undefined'></option><br/>";
echo "</select><br/><br/>";

echo "Status of power supply: <select name = 'Status'><br/>";
foreach($statuses as $status)
{
  echo "<option value='$status'>$status</option><br/>";
}
echo "</select><br/><br/>";
?>
Detector relater part: <input type="text" name="Detector"><br>

<?php
echo  "<input type='submit' value='Register Hardware' name='RegisterPS' id='RegisterPS'><br/><br/>";
if(isset($_POST["RegisterPS"])){
     if (empty($_POST["Sublocation"])) $psutil -> registerHW($_POST["SN"],$_POST["BARCODE"], $_POST["PSType"],$_POST["ESS"],$_POST["Detector"], $_POST["Location"], '', $_POST["Status"]);  
     else $psutil -> registerHW($_POST["SN"],$_POST["BARCODE"], $_POST["PSType"],$_POST["ESS"],$_POST["Detector"], $_POST["Location"], $_POST["Sublocation"], $_POST["Status"]);  
    }

?>
</form>
</div>
</div>

<!---New type--->
<div class="box" id="box3">
<H3>Register new hardware type</H3>
<!---<button  id="buttonopen3" onclick="openForm('newType', 'box3','buttonopen3','buttonclose3')">Click here to register new type </button><br/>--->
<div class="form-popup" id="newType">
<form method="post">
TYPE: <input type="text" name="TYPE"><br>
<input type='submit' value='Register new type' name='RegisterNewType' id='RegisterNewType'>
<?php
if (isset($_POST["RegisterNewType"]))$psutil -> addNewHWType($_POST["TYPE"]);
?>
</form>
<!---<button  id="buttonclose3" onclick="closeForm('newType','box3','buttonopen3','buttonclose3')">Close</button><br/>--->
</div>
</div>

<!---New location--->
<div class="bigbox" id="box4">
<H3>Register new location type</H3>
<!---<button onclick="openForm('registerLocation', 'box4','buttonopen4','buttonclose4')" id="buttonopen4">Click here to register new location</button>--->
<div class="form-popup" id="registerLocation">
<form method="post" display="none">
Location: <input type="text" name="LOCATION"><br>
<input type='submit' value='Register new location' name='RegisterNewLocation' id='RegisterNewLocation'>
<?php
if (isset($_POST["RegisterNewLocation"]))$psutil -> addNewLocation($_POST["LOCATION"]);
?>
</form>
<H4> Add new sublocation </H4>
<form method="post" display="none">
<?php
echo "Location: <select name = 'SelectLocationForSublocation'><br/>";
foreach($locations as $location)
{
  echo "<option value='$location'>$location</option><br/>";
}
echo "</select><br/>";
echo "Sublocation: <input type='text' name='Sublocation'><br>";
echo "<input type='submit' value='Add new sublocation' name='RegisterNewSublocation' id='RegisterNewSublocation'>";
if (isset($_POST["RegisterNewSublocation"]))$psutil -> addSublocation($_POST["SelectLocationForSublocation"],$_POST["Sublocation"]);
?>
<!---<button  id="buttonclose4" onclick="closeForm('registerLocation','box4','buttonopen4','buttonclose4')">Close</button><br/>--->
</div>
</div>
<script>
   function callUpdateSelected(labelsublocation, labellocation){
    var sublocations = <?php echo json_encode($sublocationmap); ?>;
    var allsublocations = <?php echo json_encode($allsublocations); ?>;
    var locations = <?php echo json_encode($locations); ?>;
    console.log(sublocations);
    updateSublocations(sublocations, allsublocations, locations, labelsublocation, labellocation);
}
</script>

<script>
  var isMember = <?php echo json_encode($isMemberOfEgroup); ?>;
  if (isMember)ApplyAccessControlForRegistration();
</script>



</body>
</html>