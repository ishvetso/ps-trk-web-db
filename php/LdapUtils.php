<?php

function isCurrentUserMemberOfEgroup($egroupToSearch)
{
  $ds=ldap_connect("xldap.cern.ch");  // must be a valid LDAP server!

if ($ds) { 
    $r=ldap_bind($ds);     // this is an "anonymous" bind, typically
    // Search surname entry
  
    //trim "CERN\" from the username
     $username = $_SERVER["REMOTE_USER"];  
    $sr=ldap_search($ds, "OU=Users,OU=Organic Units,DC=cern,DC=ch", "CN=" . $username );  
  

    $info = ldap_get_entries($ds, $sr);

    for ($i=0; $i<$info["count"]; $i++) { 
      foreach($info[$i]["memberof"] as $group) {
       if (! (strpos($group, $egroupToSearch.",OU=e-groups")) == false) return TRUE;
      }
    }

    ldap_close($ds);
    return FALSE;

} else {
    echo "<h4>Unable to connect to LDAP server</h4>";
}
}
?>​
