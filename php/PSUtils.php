
<?php


class PSUtils{

      
      function __construct(){
      global $conn;
      $dbServerName = "dbod-pstrackerdb.cern.ch";
      $dbUsername = "trkpsdbuser";
      $dbPassword = "trkHpQ72";
      $dbName = "PSTRACKERDB";
      //if debuglevel =1, messages are print out, otherwise not
      global $debuglevel;
      $debuglevel = 0;

    
      $conn = mysqli_connect($dbServerName, $dbUsername, $dbPassword,$dbName, "5501");
      if (!$conn) {
	  die('Something went wrong while connecting to mysql server<br/>');
	}

      }
      //this is a function to get the result of the query 'SELECT' which should have only *one* row as output
      function getQueryResultForOneEntry($sql, $columnName){
      	$result = mysqli_query($GLOBALS['conn'], $sql);
        
	if (!$result and $GLOBALS['debuglevel']==1) {
	  echo ("Query $sql didn't work:" . mysqli_error($conn).".<br/>");
	}
	$return =  array();
	$fetch = array();
	if (empty($result)) {
	      if ($GLOBALS['debuglevel']==1) echo "Result of $sql is empty";
	      return NULL;
	}	
	while ($fetch =  mysqli_fetch_array($result)){
	  array_push($return, $fetch["$columnName"]);
	  }
	 if (count($return)>1 ) {
	  if ($GLOBALS['debuglevel']==1) echo "The number entries in the query $sql is more than one.<br/>";
	  return NULL;
	 }
	 elseif (count($return)==0 ){
	  if ($GLOBALS['debuglevel']==1) echo("The number entries in the query $sql is zero.<br/>"); 
	  return NULL;
	 }
	 
	 else return $return[0];
	}
      
      //this is a function that gets the result of the query 'SELECT' and pushes it to array, only *one* row as output
      function getQueryResultToArray($sql, $columnName){
      	$result = mysqli_query($GLOBALS['conn'], $sql);
  
	if (!$result and $GLOBALS['debuglevel']=1) {
	  echo ("Query $sql didn't work:" . mysqli_error($conn).".<br/>");
	}
	$return =  array();
	$fetch = array();
	while ($fetch =  mysqli_fetch_array($result)){
	  array_push($return, $fetch["$columnName"]);
	  }

	 if (count($return)==0 and $GLOBALS['debuglevel']==1){
	  echo("The number entries in the query $sql is zero.<br/>"); 
	 }
	 
	 return $return;
	}
      
      function getPSTypes(){
      
	$queryPSType = "SELECT PSTYPE FROM PSTYPE2ID";
	$PSTYPES = PSUtils::getQueryResultToArray($queryPSType, "PSTYPE");
	return $PSTYPES;
       }
       
       function getLocations(){
	$queryLocation = "SELECT LOCATION FROM LOCATION2ID";
	$LOCATIONS =  PSUtils::getQueryResultToArray($queryLocation,"LOCATION");
	return $LOCATIONS;
       }
       
       function getSubLocationsForLocation($location){
	$sql = "SELECT SUBLOCATION FROM SUBLOCATION2ID WHERE LOCATIONID=LOCATION2ID('$location')";
        $SUBLOCATIONS =  PSUtils::getQueryResultToArray($sql,"SUBLOCATION");
	return $SUBLOCATIONS;
       }
       
       function getStatuses(){
	$query = "SELECT STATUS FROM STATUS2ID";
	$STATUSES =  PSUtils::getQueryResultToArray($query,"STATUS");
	return $STATUSES;
       }
       
       function getPSTypeIdByName($PSTYPE){
	$sql = "SELECT PSTYPEID FROM PSTYPE2ID WHERE PSTYPE='$PSTYPE'";
	$PSTYPEID = PSUtils::getQueryResultForOneEntry($sql, "PSTYPEID");
	return $PSTYPEID;
       }
       function getStatusIdByName($STATUS){
	$sql = "SELECT STATUSID FROM STATUS2ID WHERE STATUS='$STATUS'";
	$STATUSID = PSUtils::getQueryResultForOneEntry($sql, "STATUSID");
	return $STATUSID;
       }
      
      function getLocationIdByName($LOCATIONNAME){
       $sql = "SELECT LOCATIONID FROM LOCATION2ID WHERE LOCATION='$LOCATIONNAME'";
       $locationid = PSUtils::getQueryResultForOneEntry($sql,"LOCATIONID");
       return $locationid;
       }
       
      function getLocationNameById($LOCATIONID){
       $sql = "SELECT LOCATION FROM LOCATION2ID WHERE LOCATIONID='$LOCATIONID'";
       $location = PSUtils::getQueryResultForOneEntry($sql,"LOCATION");
       return $location;
       }
       
        function getSublocationNameById($SUBLOCATIONID){
        $sql = "SELECT SUBLOCATION FROM SUBLOCATION2ID WHERE SUBLOCATIONID='$SUBLOCATIONID'";
        $sublocation = PSUtils::getQueryResultForOneEntry($sql,"SUBLOCATION");
        return $sublocation;
       }
       
      function getPSTypeNameById($PSTYPEID){
       $sql = "SELECT PSTYPE FROM PSTYPE2ID WHERE PSTYPEID='$PSTYPEID'";
       $pstype = PSUtils::getQueryResultForOneEntry($sql,"PSTYPE");
       return $pstype;
      }
      //function returns a unique id of a hardware piece
      function getPSIDBySerialNumber($SN, $PSTYPEID){
      if (empty($SN)){
	echo "You have to enter serial number!";
	return;
      }
      $sql = "SELECT ID FROM POWERSUPPLYDATA JOIN PSTYPE2ID ON PSTYPE2ID.PSTYPEID = POWERSUPPLYDATA.PSTYPEID WHERE SERIALNUMBER=$SN and POWERSUPPLYDATA.PSTYPEID=$PSTYPEID";
      $ID = PSUtils::getQueryResultForOneEntry($sql,"ID");
      return $ID;
      }
       
      function registerHW($SN, $Barcode, $PSTYPE, $ESSNUMBER, $DETECTORPART, $LOCATION, $SUBLOCATION, $STATUS){
      if ($ESSNUMBER==NULL) $sql  = "CALL REGISTERHW ($SN, $Barcode, '$PSTYPE' , NULL, '$DETECTORPART' , '$LOCATION', '$SUBLOCATION', '$STATUS')";
      else $sql  = "CALL REGISTERHW ($SN, $Barcode, '$PSTYPE' , $ESSNUMBER, '$DETECTORPART' , '$LOCATION', '$SUBLOCATION', '$STATUS')";
       if (mysqli_query($GLOBALS['conn'], $sql)){
	 echo "<script type='text/javascript'>alert('Hardware of the type $PSTYPE with serial number $SN has been registered successfully!');</script>";
	}
	else {
		echo $sql;
		echo "<script type='text/javascript'>alert('Eror when registering hardware : " . mysqli_error($GLOBALS['conn'].");</script>");
	}
       }
       
       function getData($SN, $BARCODE, $PSTYPE,$LOCATION, $STATUS){
       $sql = " SELECT SERIALNUMBER, BARCODE,ESSLHCSERIALNUMBER, DETECTORPART, PSTYPE, LOCATION, SUBLOCATION, STATUS FROM POWERSUPPLYDATA JOIN PSLOCATION ON PSLOCATION.PSID=POWERSUPPLYDATA.ID  JOIN LOCATION2ID ON PSLOCATION.CURRENTLOCATIONID=LOCATION2ID.LOCATIONID LEFT JOIN SUBLOCATION2ID ON PSLOCATION.CURRENTSUBLOCATIONID=SUBLOCATION2ID.SUBLOCATIONID JOIN PSTYPE2ID ON PSTYPE2ID.PSTYPEID=POWERSUPPLYDATA.PSTYPEID JOIN PSSTATUS_LV ON POWERSUPPLYDATA.ID=PSSTATUS_LV.PSID JOIN STATUS2ID ON STATUS2ID.STATUSID=PSSTATUS_LV.STATUSID";
       $condition = " WHERE";
      
	if (!empty($SN)) {
	$sql=$sql.$condition." SERIALNUMBER=$SN ";
	
	if (!empty($BARCODE)) $sql = $sql." AND BARCODE=$BARCODE ";
	if (!empty($PSTYPE) and ($PSTYPE != "*") ) $sql = $sql." AND PSTYPE='$PSTYPE'";
	if (!empty($LOCATION) and ($LOCATION != "*") ) $sql = $sql." AND LOCATION='$LOCATION'";
	if (!empty($STATUS) and  ($STATUS !="*") )  $sql = $sql." AND STATUS='$STATUS'";
	
       }
        
        if (!empty($BARCODE) and empty($SN)) {
	$sql=$sql.$condition." BARCODE=$BARCODE ";
	if (!empty($PSTYPE) and ($PSTYPE!= "*")) $sql = $sql . " AND PSTYPE='$PSTYPE'";
	if (!empty($LOCATION) and ($LOCATION!= "*")) $sql = $sql . " AND LOCATION='$LOCATION'";
	if (!empty($STATUS) and ($STATUS!= "*")) $sql = $sql . " AND STATUS='$STATUS'";
	
       }
       
       if (!empty($PSTYPE) and ($PSTYPE!="*") and empty($SN) and empty($BARCODE) ) {
	$sql=$sql.$condition." PSTYPE='$PSTYPE' ";
	if (!empty($LOCATION) and ($LOCATION!= "*")) $sql = $sql . " AND LOCATION='$LOCATION'";
	if (!empty($STATUS) and ($STATUS!= "*")) $sql = $sql . " AND STATUS='$STATUS'";
       }
       
       if (!empty($LOCATION) and ($LOCATION!="*") and ($PSTYPE =="*" or empty($PSTYPE)) and empty($SN) and empty($BARCODE) ) {
	$sql=$sql.$condition." LOCATION='$LOCATION' ";
	if (!empty($STATUS) and ($STATUS!= "*")) $sql = $sql . " AND STATUS='$STATUS'";
       }
       if (!empty($STATUS) and ($STATUS!="*")  and ($LOCATION =="*" or empty($LOCATION)) and ($PSTYPE =="*" or empty($PSTYPE)) and empty($SN) and empty($BARCODE) ) $sql=$sql.$condition." STATUS='$STATUS' ";
       
       $data = array();
       $SNs = array();
       $BARCODES = array();
       $LHCSNs = array();
       $DetectorParts = array();
       $PSTYPES  = array();
       $PSLOCATIONS = array();
       $PSSUBLOCATIONS = array();
       $STATUSES = array();
       $result = mysqli_query($GLOBALS['conn'], $sql);
	if (!$result) {
	  echo "Error when retrieving the data" . mysqli_error($conn)."<br/>";
	}
	while ($fetch =  mysqli_fetch_array($result)){
	  array_push($SNs, $fetch['SERIALNUMBER']);
	  array_push($BARCODES, $fetch['BARCODE']);
	  array_push($LHCSNs, $fetch['ESSLHCSERIALNUMBER']);
	  array_push($DetectorParts, $fetch['DETECTORPART']);
	  array_push($PSTYPES, $fetch['PSTYPE']);
	  array_push($PSLOCATIONS, $fetch['LOCATION']);
	  array_push($PSSUBLOCATIONS, $fetch['SUBLOCATION']);
	  array_push($STATUSES, $fetch['STATUS']);
	  }
	  $data['BARCODE'] = $BARCODES;
	  $data['SERIALNUMBER'] = $SNs;
	  $data['LHCSERIALNUMBER'] = $LHCSNs;
	  $data['PSTYPES'] = $PSTYPES;
	  $data['LOCATIONS'] = $PSLOCATIONS;
	  $data['SUBLOCATIONS'] = $PSSUBLOCATIONS;
	  $data['STATUSES'] = $STATUSES;
	  $data['DETECTORPART'] = $DetectorParts;
        return $data;
       }
       
       function executeTransaction($SN, $PSTYPE, $LOCATIONOUT, $SUBLOCATIONOUT, $LOCATIONIN, $SUBLOCATIONIN, $COMMENT){
        if (empty($SN)){
	  echo "You have to enter serial number. Transaction has not been completed.";
	  return;
        }
        $sublocations =  PSUtils::getSubLocationsForLocation($LOCATIONIN);
        if ($LOCATIONOUT == $LOCATIONIN){
	    //if there are no sublocations locations must be different
            if (count($sublocations)==0) {
	      echo "<script type='text/javascript'>alert('Locations must be different. The transaction has not been executed.');</script>";
	      return;
	    }
	    if (count($sublocations)>0 && $SUBLOCATIONIN == $SUBLOCATIONOUT) {
	      echo "<script type='text/javascript'>alert('Sublocations must be different. The transaction has not been executed.');</script>";
	      return;
	    }
	    
        }
        $correctsublocation = false;
	for ($i = 0; $i<count($sublocations);$i++){
	    if ($sublocations[$i] == $SUBLOCATIONIN) $correctsublocation = true;
	  }
	
	if (!$correctsublocation && !empty($SUBLOCATIONIN)  && count($sublocations)>0  ) {
	    echo "<script type='text/javascript'>alert('The specified sublocation is not available for the specified location.');</script>";
	    return;
	  }
	  
        $location = PSUtils::getCurrentLocation($SN,$PSTYPE);
        if ($location != $LOCATIONOUT){
	  echo "location $LOCATIONOUT doesn't correspond to the one stored in the database at the moment. <br/> ";
	  return;
        }
        if (empty($COMMENT)){
	  echo "You have to specify a description for the transaction. <br/>";
	  return;
        }
	$PSTYPEID = PSUtils::getPSTypeIdByName($PSTYPE);
	$ID = PSUtils::getPSIDBySerialNumber($SN, $PSTYPEID);
	$LOCATIONIDOUT = PSUtils::getLocationIdByName($LOCATIONOUT);
	$LOCATIONIDIN = PSUtils::getLocationIdByName($LOCATIONIN);
	$sql = "INSERT INTO PSTRANSACTIONS (PSID, LOCATIONIDOUT, LOCATIONIDIN, SUBLOCATIONIDIN, SUBLOCATIONIDOUT, DATETIME, PROBLEMDESC) VALUES ($ID,$LOCATIONIDOUT,$LOCATIONIDIN, SUBLOCATION2ID('$SUBLOCATIONIN'), SUBLOCATION2ID('$SUBLOCATIONOUT'), NOW(), '$COMMENT')";
	
	if (mysqli_query($GLOBALS['conn'], $sql)){
	 echo "<script type='text/javascript'>alert('The board $PSTYPE with serial number $SN has been moved from $LOCATIONOUT $SUBLOCATIONOUT to $LOCATIONIN $SUBLOCATIONIN successfully.');</script>";
	}
	
	else {
		 echo "<script type='text/javascript'>alert('Eror when submitting transaction : . mysqli_error($conn)');</script>";
	}

       }
       function getCurrentLocation($SN, $PSTYPE){
	if (empty($SN) or empty($PSTYPE)){
	  echo "Empty parameters passed to getCurrentLocation()<br/>";
	  return;
	}
	$sql = "SELECT SERIALNUMBER, BARCODE, PSTYPE, LOCATION FROM POWERSUPPLYDATA JOIN PSLOCATION ON PSLOCATION.PSID=POWERSUPPLYDATA.ID  JOIN LOCATION2ID ON PSLOCATION.CURRENTLOCATIONID=LOCATION2ID.LOCATIONID JOIN PSTYPE2ID ON PSTYPE2ID.PSTYPEID=POWERSUPPLYDATA.PSTYPEID WHERE SERIALNUMBER=$SN and PSTYPE='$PSTYPE'";
	$location = PSUtils::getQueryResultForOneEntry($sql, "LOCATION");
	return $location;
       }
       function addNewHWType($NEWTYPE){
        if (empty($NEWTYPE)){
	  echo "A hardware type must be non empty. <br/>";
	  return;
        }
	$sql = "INSERT INTO PSTYPE2ID (PSTYPE) VALUES ('$NEWTYPE')";
	
	if (mysqli_query($GLOBALS['conn'], $sql)){
	  echo "The type $NEWTYPE added successfully!";
	}
	
	else {
	  echo "Eror when adding new type $NEWTYPE. " . mysqli_error($conn);
	}
 
       }
       
        function addNewLocation($NEWLOCATION){
        if (empty($NEWLOCATION)){
	  echo "A location name must be non empty. <br/>";
	  return;
        }
	$sql = "INSERT INTO LOCATION2ID (LOCATION) VALUES ('$NEWLOCATION')";
	
	if (mysqli_query($GLOBALS['conn'], $sql)){
	  echo "The location $NEWLOCATION added successfully!";
	}
	else {
	  echo "Eror when adding new location $NEWLOCATION. " . mysqli_error($conn);
	}
       }
       function addSublocation($LOCATION, $NEWSUBLOCATION){
	  $locationexists = false;
	  if (empty($NEWSUBLOCATION)) {
	    echo "<script type='text/javascript'>alert('A sublocation must be non empty string. No sublocation has been registered.');</script>";	
	    return;
	  }
	  $checklocation = "SELECT LOCATION FROM LOCATION2ID WHERE LOCATION='$LOCATION'";
	  $result = PSUtils::getQueryResultForOneEntry($checklocation, "LOCATION");
	  if (empty($result)) {
	      echo "<script type='text/javascript'>alert('The location specified does not exist. No sublocation has been registered.');</script>";
	     return;
	  }
	  
	  $checksublocation = "SELECT SUBLOCATION FROM SUBLOCATION2ID WHERE SUBLOCATION='$NEWSUBLOCATION'";
	  $result = PSUtils::getQueryResultForOneEntry($checksublocation, "SUBLOCATION");
	  if (!empty($result)) {
	    echo "<script type='text/javascript'>alert('The sublocation specified already exists. No sublocation has been registered.');</script>";
	     return;
	  }
	  
	  $sql = "INSERT INTO SUBLOCATION2ID (SUBLOCATION, LOCATIONID) VALUES ('$NEWSUBLOCATION', LOCATION2ID('$LOCATION'))";
	
	  if (mysqli_query($GLOBALS['conn'], $sql)){
	  echo "<script type='text/javascript'>alert('The sublocation $NEWSUBLOCATION added successfully!');</script>";
	     return;
	  }
	  else {
	    echo "Eror when adding new sublocation $NEWSUBLOCATION. " . mysqli_error($conn);
	  }
	  
       }
              
       function updateStatus($SN, $PSTYPE, $NEWSTATUS, $COMMENT){
        if (empty($SN)){
	  echo "You have to enter serial number.";
	  return;
        }
	if ($NEWSTATUS == PSUtils::getCurrentStatus($SN, $PSTYPE)){
	    echo "Status should differ from the current one.";
	    return;
	  }
	$PSTYPEID = PSUtils::getPSTypeIdByName($PSTYPE);
	$ID = PSUtils::getPSIDBySerialNumber($SN, $PSTYPEID);
	$STATUSID = PSUtils::getStatusIdByName($NEWSTATUS);
	$sql = "INSERT INTO  PSSTATUS (PSID, STATUSID, SINCE, COMMENT) VALUES($ID, $STATUSID,NOW(), '$COMMENT')";
	
	if (mysqli_query($GLOBALS['conn'], $sql)){
	  echo "<script type='text/javascript'>alert('Status of board $PSTYPE with serial number $SN has been change to $NEWSTATUS successfully.');</script>";
	}
	
	else {
	  echo "Eror when submitting transaction $sql " . mysqli_error($conn);
	}
       }
       
       function getBarcode($SN, $PSTYPE){
        
        $PSTYPEID = PSUtils::getPSTypeIdByName($PSTYPE);
	$ID = PSUtils::getPSIDBySerialNumber($SN, $PSTYPEID);
        $sql = "select BARCODE from POWERSUPPLYDATA JOIN PSTYPE2ID ON POWERSUPPLYDATA.PSTYPEID = PSTYPE2ID.PSTYPEID WHERE ID=$ID";
        $BARCODE = PSUtils::getQueryResultForOneEntry($sql, "BARCODE");
        return $BARCODE;
       }
       
        function getESSNumber($SN, $PSTYPE){
        
        $PSTYPEID = PSUtils::getPSTypeIdByName($PSTYPE);
	$ID = PSUtils::getPSIDBySerialNumber($SN, $PSTYPEID);
        $sql = "select ESSLHCSERIALNUMBER from POWERSUPPLYDATA WHERE ID=$ID";
        $ESS = PSUtils::getQueryResultForOneEntry($sql, "ESSLHCSERIALNUMBER");
        return $ESS;
       }

       function getEPoolIternalID($SN, $PSTYPE){
        
        $PSTYPEID = PSUtils::getPSTypeIdByName($PSTYPE);
		$ID = PSUtils::getPSIDBySerialNumber($SN, $PSTYPEID);
        $sql = "select ITEM_ID_EPOOL from POWERSUPPLYDATA WHERE ID=$ID";
        $ESS = PSUtils::getQueryResultForOneEntry($sql, "ITEM_ID_EPOOL");
        return $ESS;
       }
       
        function getDetectorPart($SN, $PSTYPE){
        
        $PSTYPEID = PSUtils::getPSTypeIdByName($PSTYPE);
	$ID = PSUtils::getPSIDBySerialNumber($SN, $PSTYPEID);
        $sql = "select DETECTORPART from POWERSUPPLYDATA WHERE ID=$ID";
        $DETECTOR = PSUtils::getQueryResultForOneEntry($sql, "DETECTORPART");
        return $DETECTOR;
       }
       
        function getLocation($SN, $PSTYPE){
        
        $PSTYPEID = PSUtils::getPSTypeIdByName($PSTYPE);
	$ID = PSUtils::getPSIDBySerialNumber($SN, $PSTYPEID);
        $sql = "select LOCATION from POWERSUPPLYDATA JOIN PSLOCATION ON POWERSUPPLYDATA.ID = PSLOCATION.PSID JOIN LOCATION2ID ON PSLOCATION.CURRENTLOCATIONID = LOCATION2ID.LOCATIONID WHERE ID=$ID";
        $LOCATION = PSUtils::getQueryResultForOneEntry($sql, "LOCATION");
        return $LOCATION;
       }
       
        function getSublocation($SN, $PSTYPE){
        
        $PSTYPEID = PSUtils::getPSTypeIdByName($PSTYPE);
	$ID = PSUtils::getPSIDBySerialNumber($SN, $PSTYPEID);
        $sql = "select SUBLOCATION from POWERSUPPLYDATA JOIN PSLOCATION ON POWERSUPPLYDATA.ID = PSLOCATION.PSID JOIN SUBLOCATION2ID ON PSLOCATION.CURRENTSUBLOCATIONID = SUBLOCATION2ID.SUBLOCATIONID WHERE ID=$ID";
        $SUBLOCATION = PSUtils::getQueryResultForOneEntry($sql, "SUBLOCATION");
         if ($SUBLOCATION == NULL) return "";
        else return $SUBLOCATION;
       }
        
        function getCurrentStatus($SN, $PSTYPE){
        
        $PSTYPEID = PSUtils::getPSTypeIdByName($PSTYPE);
	$ID = PSUtils::getPSIDBySerialNumber($SN, $PSTYPEID);
        $sql = "select STATUS from PSSTATUS_LV JOIN STATUS2ID ON PSSTATUS_LV.STATUSID = STATUS2ID.STATUSID  WHERE PSID=$ID";
        $STATUS = PSUtils::getQueryResultForOneEntry($sql, "STATUS");
        //getting the latest status
        return $STATUS;
       }
       
       function getStatusHistory($SN, $PSTYPE){
        
        $PSTYPEID = PSUtils::getPSTypeIdByName($PSTYPE);
	$ID = PSUtils::getPSIDBySerialNumber($SN, $PSTYPEID);
        $sql = "select STATUS, SINCE, COMMENT from PSSTATUS JOIN STATUS2ID ON PSSTATUS.STATUSID = STATUS2ID.STATUSID  WHERE PSID=$ID ORDER BY SINCE DESC";
	
	$result = mysqli_query($GLOBALS['conn'], $sql);
  
	if (!$result) {
	  echo ("Query $sql didn't work:" . mysqli_error($conn).".<br/>");
	}
	$statuses =  array();
	$comments = array();
	$times = array();
	$fetch = array();
	while ($fetch =  mysqli_fetch_array($result)){
	  array_push($statuses, $fetch["STATUS"]);
	  array_push($times, $fetch["SINCE"]);
	  array_push($comments, $fetch["COMMENT"]);
	  }
	$history = array();
	$history["TIME"] = $times;
	$history["STATUS"] = $statuses;
	$history["COMMENT"] = $comments;

	 return $history;
       }
       
       function getTransactionHistory($SN, $PSTYPE){
        
        $PSTYPEID = PSUtils::getPSTypeIdByName($PSTYPE);
	$ID = PSUtils::getPSIDBySerialNumber($SN, $PSTYPEID);
        $sql = "select LOCATIONIDIN, LOCATIONIDOUT, DATETIME, PROBLEMDESC, SUBLOCATIONIDOUT, SUBLOCATIONIDIN, HISTORIC from PSTRANSACTIONS  WHERE PSID=$ID ORDER BY DATETIME DESC";
	
	$result = mysqli_query($GLOBALS['conn'], $sql);
  
	if (!$result) {
	  echo ("Query $sql didn't work:" . mysqli_error($conn).".<br/>");
	}
	$locationsin =  array();
	$locationsout = array();
	$sublocationin = array();
	$sublocationout = array();
	$times = array();
	$comments = array();
	$historic = array();
	$fetch = array();
	while ($fetch =  mysqli_fetch_array($result)){
	  array_push($locationsin, PSUtils::getLocationNameById($fetch["LOCATIONIDIN"]));
	  array_push($locationsout, PSUtils::getLocationNameById($fetch["LOCATIONIDOUT"]));
	  array_push($sublocationin, PSUtils::getSublocationNameById($fetch["SUBLOCATIONIDIN"]));
	  array_push($sublocationout, PSUtils::getSublocationNameById($fetch["SUBLOCATIONIDOUT"]));
	  array_push($historic, $fetch["HISTORIC"]);
	  array_push($comments, $fetch["PROBLEMDESC"]);
	  array_push($times, $fetch["DATETIME"]);
	  }
	$history = array();
	$history["LOCATIONIN"] = $locationsin;
	$history["LOCATIONOUT"] = $locationsout;
	$history["SUBLOCATIONIN"] = $sublocationin;
	$history["SUBLOCATIONOUT"] = $sublocationout;
	$history["HISTORIC"]  = $historic;
	$history["COMMENT"] = $comments;
	$history["TIME"] = $times;
	
	return $history;
       }
       
       function getNumberPSTypeWithStatusUntilDate($date, $status, $type)
       {
        $function = "COUNTPSWITHSTATUS(STR_TO_DATE('$date 10:00','%d.%m.%Y %h:%i'), '$status', '$type')";
	$sql = "SELECT $function";
        $count = PSUtils::getQueryResultForOneEntry($sql, $function);
        //getting the latest status
        return $count;
       }
       
       function getNumberPSStatistics( $status, $type)
       {
        $function = "COUNTPSSTATISTICS('$status', '$type')";
	$sql = "SELECT $function";
        $count = PSUtils::getQueryResultForOneEntry($sql, $function);
        //getting the latest status
        return $count;
       }
       
       function updateDetectorPart($SN, $PSTYPE, $NEWDETECTORPART){ 
	$PSTYPEID = PSUtils::getPSTypeIdByName($PSTYPE);
	$ID = PSUtils::getPSIDBySerialNumber($SN, $PSTYPEID);
	
	$sql = "UPDATE POWERSUPPLYDATA SET  DETECTORPART='$NEWDETECTORPART' WHERE ID=$ID";
	
	if (mysqli_query($GLOBALS['conn'], $sql)){
	    echo "<script type='text/javascript'>alert('Detetor related part has been updated to $NEWDETECTORPART successfully.');</script>";
	}
	
	else {
	  echo "Eror  updating detector related part  " . mysqli_error($conn);
	  }
	}

	function updateLHCSN($SN, $PSTYPE, $NEWLHCSN){ 
	$PSTYPEID = PSUtils::getPSTypeIdByName($PSTYPE);
	$ID = PSUtils::getPSIDBySerialNumber($SN, $PSTYPEID);

	$queryToFindLHCSN = "SELECT ESSLHCSERIALNUMBER FROM POWERSUPPLYDATA WHERE ID=$ID";
	$ESSSN = PSUtils::getQueryResultForOneEntry($queryToFindLHCSN, "ESSLHCSERIALNUMBER");

	if ($ESSSN !=NULL)  {
		echo "<script type='text/javascript'>alert('LHC serial number already exists for this board.');</script>";
		return;
	}
	
	$sql = "UPDATE POWERSUPPLYDATA SET  ESSLHCSERIALNUMBER='$NEWLHCSN' WHERE ID=$ID";
	
	if (mysqli_query($GLOBALS['conn'], $sql)){
	    echo "<script type='text/javascript'>alert('LHC serial number has been added successfully.');</script>";
	}
	
	else {
	  echo "Eror  adding LHC serial number " . mysqli_error($conn);
	  }
	}

	 function getAllQualifications(){
	$query = "SELECT QUALIFICATIONNAME FROM QUALIFICATION_MAP";
	$qualifications =  PSUtils::getQueryResultToArray($query,"QUALIFICATIONNAME");
	return $qualifications;
       }


//insert to QUALIFICATION_DATA TABLE
    function addQualificationInformation($SN, $PSTYPE, $qualificationtest,$passed, $username, $comment){ 
	$PSTYPEID = PSUtils::getPSTypeIdByName($PSTYPE);
	$ID = PSUtils::getPSIDBySerialNumber($SN, $PSTYPEID);
	
	$sql = "INSERT INTO QUALIFICATION_DATA (PSID, QUALIFICATIONID, PASSEDBIT, PASSEDTIME, WHOID, COMMENT) VALUES ($ID, QUALIFICATIONNAME2ID('$qualificationtest'), $passed, NOW(), USERNAME2ID('$username'), '$comment' )";
	if (mysqli_query($GLOBALS['conn'], $sql)) return 1;
	
	else {
	  echo "Error  occured adding qualification information! " . mysqli_error($conn);
	  return 0;
	  }
	}


	function getQualificationHistory($SN, $PSTYPE){
        
    $PSTYPEID = PSUtils::getPSTypeIdByName($PSTYPE);
	$ID = PSUtils::getPSIDBySerialNumber($SN, $PSTYPEID);
    $sql = "SELECT QUALIFICATIONID2NAME(QUALIFICATIONID), PASSEDBIT, PASSEDTIME, USERID2FULLNAME(WHOID),COMMENT FROM QUALIFICATION_DATA  WHERE PSID=$ID ORDER BY PASSEDTIME DESC";
	
	$result = mysqli_query($GLOBALS['conn'], $sql);
  
	if (!$result) {
	  echo ("Query $sql didn't work:" . mysqli_error($conn).".<br/>");
	}
	$qualifications_ =  array();
	$passedbits = array();
	$passedtimes = array();
	$responsibles = array();
	$comments = array();
	$fetch = array();
	while ($fetch =  mysqli_fetch_array($result)){
	  array_push($qualifications_, $fetch["QUALIFICATIONID2NAME(QUALIFICATIONID)"]);
	  array_push($passedbits, $fetch["PASSEDBIT"]);
	  array_push($passedtimes, $fetch["PASSEDTIME"]);
	  array_push($responsibles, $fetch["USERID2FULLNAME(WHOID)"]);
	  array_push($comments, $fetch["COMMENT"]);
	  }
	$history = array();
	$history["QUALIFICATION"] = $qualifications_;
	$history["PASSEDBIT"] = $passedbits;
	$history["PASSEDTIME"] = $passedtimes;
	$history["RESPONSIBLE"] = $responsibles;
	$history["COMMENT"] = $comments;

	 return $history;
       }



    function getQualificationIdByName($qualification){
	$sql = "SELECT QUALIFICATIONID FROM QUALIFICATION_MAP WHERE QUALIFICATIONNAME='$qualification'";
	$qualificationid = PSUtils::getQueryResultForOneEntry($sql, "QUALIFICATIONID");
	return $qualificationid;
    }



}
?>

