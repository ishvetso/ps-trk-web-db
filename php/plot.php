<?php
 ini_set('display_errors', 1);
 ini_set('display_startup_errors', 1);
 error_reporting(E_ALL);
 require 'PSUtils.php';
 $psutil = new PSUtils();
 $locations = $psutil-> getLocations();
 $statuses = $psutil-> getStatuses();
 $pstypes = $psutil-> getPSTypes();
  date_default_timezone_set('UTC');
 
?>


<html> 
<head>
<script src="../js/plotly-latest.min.js"></script>
</head>

<body>
 <a href='../parsetable.php'>  Go to main page</a>
<H1> Rate of broken hardware </H1>
<p>Note: supported time range is from 2010-01-01 till 2024-12-31.</p>
<form method="post">
Power supply type: <select id = "SelectedTypes" multiple>
<?php
foreach($pstypes as $pstype)
{
  echo "<option value='$pstype'>$pstype</option><br/>";
}
echo "</select><br/>";
?>
<label for="start">Start date:</label>
<input type="date" id="start" value="2010-01-01" min="2010-01-01" max="2024-12-31"><br>
<label for="end">End date:</label>
<input type="date" id="end"  value="2020-01-01" min="2010-01-01" max="2024-12-31"><br>

<?php
echo  "<input type='button' value='Plot' name='MyPlot' onclick=plot()><br/><br/>";
?>
</form>
  <div id="tester" style="width:1000px;height:700px;">
  </div>
  
<?php
$DateStart = "2010-01-01";
$DateEnd = "2024-12-31";

$timeseries = array();
$integrals = array();


foreach($pstypes as $pstype)
{
 $integrals[$pstype] = array();
}

for ($i=0; $i< count($pstypes); $i++) {

  $daterestart = $DateStart;
    while ($daterestart < $DateEnd){
    $daterestart = date('Y-m-d', strtotime($daterestart. ' + 15 days'));
    $time = date_format(new DateTime($daterestart),"d.m.Y");
    //this format is needed for javascript, as far as i understand (at least it didn't work with the other date format)
    $time_ = date_format(new DateTime($daterestart),"Y-m-d");
    $count = $psutil -> getNumberPSTypeWithStatusUntilDate($time,"broken",$pstypes[$i]);
    if($i==0)array_push($timeseries, $time_);
    array_push($integrals[$pstypes[$i]], $count);
    }
}
?>
<script>
function plot(){ 
 var startdate_ = document.getElementById("start").value;
 var enddate_ = document.getElementById("end").value;
  var dates = <?php echo json_encode($timeseries); ?>;
  var selecteddates =[];

  var numbers  =  <?php echo json_encode($integrals); ?>;
  var selectednumbers = [];
  var iDate, iType;
  var mypstypes  =  <?php echo json_encode($pstypes); ?>;
  for (iType = 0; iType < mypstypes.length; iType ++) {
    selectednumbers[mypstypes[iType]] =  [];
  }
  //now we select data according to the selected time interval
  var date_;
  var startDate = new Date(startdate_);
  var endDate = new Date(enddate_);
    
    for (iDate =0; iDate < dates.length; iDate++) {
      date_ = new Date(dates[iDate]);
      if (date_ > startDate && date_ < endDate) {
       selecteddates.push(dates[iDate]);
        for (iType=0; iType < mypstypes.length; iType++){
	  selectednumbers[mypstypes[iType]].push(numbers[mypstypes[iType]][iDate]);	  
	}
     }
  }
  //shift values by the value in the first element of the array
  var k, shift;
  for (iType=0; iType < mypstypes.length; iType++){
  shift = selectednumbers[mypstypes[iType]][0];
   for (k=0; k < selectednumbers[mypstypes[iType]].length; k++){
      selectednumbers[mypstypes[iType]][k] = selectednumbers[mypstypes[iType]][k] - shift;
     }
  }
  var selectedtypes = [];
  selectedtypes = document.getElementById("SelectedTypes");
  var j;
  var i;
  var dataplots = [];
  var selected;
  var allplots;
  for (i = 0; i < mypstypes.length ; i++){
   selected = false;
   for (j =0; j< selectedtypes.length; j++){
     if (selectedtypes[j].value == mypstypes[i] && selectedtypes[j].selected) selected = true;
   }
   
   var value = {
    x: selecteddates, 
    y: selectednumbers[mypstypes[i]],
    type: 'scatter',
    name: mypstypes[i]
  };
   if (selected)dataplots.push(value);
  }

  var layout = {
    title: 'Power supply exchange rate'
  };
    TESTER = document.getElementById('tester');
    Plotly.newPlot( TESTER, dataplots, layout);
}



</script>
  
  </body>
</html>