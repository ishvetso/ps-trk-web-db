
<head>
<link rel="stylesheet" href="../css/style.css">
<script type="text/javascript" src="../js/SelectOptions.js">
</script>
<script type="text/javascript" src="../js/ApplyAccessControl.js">
</script>
<script type="text/javascript" src="../js/EnableAnswersForQualification.js">
</script>
</head>
<body onload="callUpdateSelected('SelectSublocation','SelectLocation')">
<?php
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);
 require 'PSUtils.php';
 require 'LdapUtils.php';
 $psutil = new PSUtils();
 $locations = $psutil->getLocations();
 $statuses = $psutil-> getStatuses();
 $qualifications = $psutil ->getAllQualifications();

   $username = $_SERVER["REMOTE_USER"]; 
 
  $sublocationmap = array();
  $allsublocations = array();
  $isMemberOfEgroup = isCurrentUserMemberOfEgroup("cms-trk-powersupplies-db_write");
 foreach($locations as $location){
  $sublocationmap[$location] = array();
  $sublocations = $psutil -> getSubLocationsForLocation($location);
  for ($i=0; $i<count($sublocations);$i++){
    array_push($sublocationmap[$location],$sublocations[$i]);
    array_push($allsublocations,$sublocations[$i]);
   }
 }
 
 $SN = $_REQUEST['serialnumber'];
 $PSTYPE = $_REQUEST["pstype"];
 echo "<a href='../parsetable.php'>  Go to main page</a>";
 echo "<H3> Information about power supply</H3>";
 ?>
 <div class="boxed">
 <?php
 echo "<p> Serial number: $SN </p>";
 echo "<p> Type: $PSTYPE </p>";
 $BARCODE = $psutil -> getBarcode($SN, $PSTYPE);
 $ESSnumber = $psutil -> getESSNumber($SN, $PSTYPE);
 $Detector = $psutil -> getDetectorPart($SN, $PSTYPE);
 $internalItemID  = $psutil -> getEPoolIternalID($SN, $PSTYPE);
 
 echo "<p> Barcode: $BARCODE </p>";
# echo "<p> LHC Serial number : $ESSnumber </p>";
 #here is introduced possibility to add LHC SERIAL NUMBER, in case it was not added initially
 echo "<form method='post'><br/>";
 echo "<input type='number' name='ESSnumber' value='$ESSnumber'>";
 ?>
 <input type='submit' value='Add LHC serial number (if not exists)' name='UpdateLHCSN' id="UpdateLHCSN" disabled="true">
 
 <?php
if(isset($_POST["UpdateLHCSN"])) $psutil -> updateLHCSN($SN, $PSTYPE, $_POST["ESSnumber"]);
  echo "</form><br/>";
  echo "LHC serial number:<a href=\"https://aisdb.cern.ch/pls/htmldb_aisdb_prod/f?p=117:7:216632793106348::NO:RP,7:ID_STATUS,ID_ITEM_ID:DISPLAY," .$internalItemID ."\">".$ESSnumber."</a>";

 echo "<form method='post'><br/>";
 echo "Detector part related: <input type='text' name='DETECTOR' value='$Detector'><br>";
 ?>
 <input type='submit' value='Update detector related' name='UpdateDetector' id="UpdateDetector" disabled="true">
 
 <?php
if(isset($_POST["UpdateDetector"])) $psutil -> updateDetectorPart($SN, $PSTYPE, $_POST["DETECTOR"]);
  echo "</form><br/>";

 $CURRENTLOCATION = $psutil -> getLocation($SN, $PSTYPE);
 $CURRENTSUBLOCATION = $psutil -> getSublocation($SN, $PSTYPE);

 ?>

<form method="post">
<?php
echo "Current location: <select name = 'LocationIN' id='SelectLocation' onchange=callUpdateSelected('SelectSublocation','SelectLocation')><br/>";
foreach($locations as $location)
{
  if ($location!=$CURRENTLOCATION)echo "<option value='$location'>$location</option><br/>";
  else echo "<option value='$location' selected>$location</option><br/>";
}
echo "</select><br/>";

echo "Current sublocation: <select name = 'SublocationIN' id='SelectSublocation'><br/>";
$sublocations  = $psutil -> getSubLocationsForLocation($CURRENTLOCATION);

foreach($allsublocations as $sublocation)
{
  if ($sublocation!=$CURRENTSUBLOCATION)echo "<option value='$sublocation'>$sublocation</option><br/>";
  else echo "<option value='$sublocation' selected>$sublocation</option><br/>";
}
if ($CURRENTSUBLOCATION=="")echo  "<option selected value='' label='undefined'><br>";
else echo "<option value='' label='undefined'></option><br/>";
echo "</select><br/>";
echo "Comment: <input type='text' name='COMMENT'><br>";
?>
<input type='submit' value='Change location' name='Transaction' id='Transaction' disabled="true">
<?php
if(isset($_POST["Transaction"])){
    $psutil -> executeTransaction($SN, $PSTYPE, $CURRENTLOCATION, $CURRENTSUBLOCATION, $_POST["LocationIN"], $_POST["SublocationIN"], $_POST["COMMENT"]);
    header("Refresh:20");
   
    $message = "This is an automatic e-mail to inform that: \nThe location of the " .  $PSTYPE . " board with serial number ". $SN ." has been changed from " . $CURRENTLOCATION . " " . $CURRENTSUBLOCATION . " to " . $_POST["LocationIN"] . " " . $_POST["SublocationIN"] . " with comment: " .  $_POST["COMMENT"] . "\nThe action was done by ". $username;
    mail("ivan.shvetsov@cern.ch", "PS Exchange Notification", $message);
  }
?>
</form>




<form method="post">

<?php
 $CURRENTSTATUS = $psutil -> getCurrentStatus($SN, $PSTYPE);
echo "Current status: <select name = 'StatusNew'>";
foreach($statuses as $status)
{
  if ($status != $CURRENTSTATUS)echo "<option value='$status'>$status</option><br/>";
  else echo "<option value='$status' selected>$status </option><br/>";
}
echo "</select><br/>";
echo "Comment: <input type='text' name='COMMENTSTATUS'><br>";
?>
<input type='submit' value='Update status' name='StatusUpdate' id='StatusUpdate' disabled="true">
<?php
if(isset($_POST["StatusUpdate"])){
    $psutil -> updateStatus($SN, $PSTYPE, $_POST["StatusNew"], $_POST["COMMENTSTATUS"]);
      $message = "This is an automatic e-mail to inform that: \nThe status of the " .  $PSTYPE . " board with serial number ". $SN ." has been changed from " . $CURRENTSTATUS . " to " . $_POST["StatusNew"] . " with comment: " . $_POST["COMMENTSTATUS"] . "\nThe action was done by ". $username;
    mail("ivan.shvetsov@cern.ch", "PS Exchange Notification", $message);

  }
?>
</form>

</div>
 
<?php
$historyStatus = $psutil -> getStatusHistory($SN,$PSTYPE);
$historyTransactions = $psutil -> getTransactionHistory($SN,$PSTYPE);
$allhistory = array();
echo "<div class='history'>";
 echo "<h4> History of hardware </h4>";
for ($i=0; $i<count($historyStatus['TIME']);$i++)
  {
      $time_ = strtotime($historyStatus['TIME'][$i]);
      $allhistory[$time_] =  "status changed to ".$historyStatus['STATUS'][$i] . " ". $historyStatus['COMMENT'][$i];
  }

for ($j=0; $j<count($historyTransactions['TIME']);$j++)
{
    $time_ = strtotime($historyTransactions['TIME'][$j])+1;#this is just to be sure not to overwrite same elements
    $transaction = $historyTransactions['LOCATIONOUT'][$j]. " ".$historyTransactions['SUBLOCATIONOUT'][$j] ." ---> ".$historyTransactions['LOCATIONIN'][$j]. " " .$historyTransactions['SUBLOCATIONIN'][$j] .  ": ".$historyTransactions['COMMENT'][$j] .( $historyTransactions["HISTORIC"][$j] ?  ": historic data":"");     
    $allhistory[$time_] = $transaction;
}  

ksort($allhistory);
 foreach ($allhistory as $timeval => $val) {
    echo date("d.m.Y h:i:s",$timeval)." : $val<br/>";
}


?> 
</div>

<?php
 echo "<div class='qualification'>";
 echo "<h4> Qualification history </h4>";

 $qualificationhistory = $psutil -> getQualificationHistory($SN,$PSTYPE);
for ($i=0; $i<count($qualificationhistory['PASSEDTIME']);$i++)
  {
     $result = '';
     if ( $qualificationhistory['PASSEDBIT'][$i] == 1 ) $result = "passed";
     elseif ( $qualificationhistory['PASSEDBIT'][$i] == 0 ) $result = "failed";
     else $result = 'unknown';
      echo $qualificationhistory['PASSEDTIME'][$i] . ": ". " " . $result . "  " .  $qualificationhistory['QUALIFICATION'][$i] . "; test performed by " . $qualificationhistory['RESPONSIBLE'][$i] . " with the comment ". $qualificationhistory['COMMENT'][$i]. "<br/>";
      #."  " . $qualificationhistory['PASSEDBIT'][$i] ? "passed the test ":"failed the test" . $qualificationhistory["QUALIFICATIONID2NAME(QUALIFICATIONID)"][$i];
  }
 ?>

<form method="post">
<?php
 echo "<h4> Qualification information </h4>";
foreach($qualifications as $qualification)
{
 $qualificationid = $psutil -> getQualificationIdByName($qualification);
  echo "<div id='qualification-selector". $qualification."''>"; 
  echo  "<input type='checkbox'  name='qualification[]'  value='$qualification' onclick='EnableAnswers($qualificationid)'>$qualification";
 
 $answername = 'answer'.$qualificationid.'[]';
 $yesid = 'yes'.$qualificationid;
 $noid = 'no'.$qualificationid;
 $commentqualification  = 'comment'.$qualificationid;
 echo "<div hidden = 'True' id='$yesid' > <input type='radio' name='$answername' value='Yes'>Passed</input></div>";
 echo "<div hidden = 'True' id='$noid'>  <input type='radio' name='$answername' value='No' >Failed</input></div>";
 echo "<div hidden = 'True' id='$commentqualification'> comment:<input type='text' name='$commentqualification'></input></div>";
  echo "</div>";
}
?>
<input type='submit' value='submit qualification results' name='updatequalification' id='updatequalification'>
</form>
<?php

$results = array();
if(isset($_POST["updatequalification"])){
  foreach($_POST['qualification'] as $iqualification)
  {

    $qualificationid = $psutil -> getQualificationIdByName($iqualification);
    $answername = 'answer'.$qualificationid;
    $mycomment = 'comment'.$qualificationid;

    if(!empty($_POST['qualification'])){
       if (!empty($_POST[$answername])) {
          foreach ($_POST[$answername] as $ianswer) {
            if ($ianswer=='Yes') {
             $currentresult = $psutil -> addQualificationInformation($SN, $PSTYPE, $iqualification,1, $username, $_POST[$mycomment]);       
             array_push($results, $currentresult);
            }
            elseif ($ianswer=='No'){
              $currentresult = $psutil -> addQualificationInformation($SN, $PSTYPE, $iqualification,0, $username, $_POST[$mycomment]);  
              array_push($results, $currentresult);
            }
            else  echo "<script type='text/javascript'>alert('Error occured submitting qualification results!!! No result of the test given. ');</script>";     
            } 
          }
        }     
    }
  $alltestssubmitted = true;
  foreach ($results as $iresult) {
    if ($iresult!=1) $alltestssubmitted  = false;
  }
  //here notify the user that all results were sent correctly
  if ($alltestssubmitted and sizeof($results)> 0)echo "<script type='text/javascript'>alert('All qualification results has been submitted.');</script>";       
  }

?>




</div>
<script>
	var isMember = <?php echo json_encode($isMemberOfEgroup); ?>;
	if (isMember)ApplyAccessControl();
</script>
<script>
function callUpdateSelected(labelsublocation, labellocation){
    var sublocations = <?php echo json_encode($sublocationmap); ?>;
    var allsublocations = <?php echo json_encode($allsublocations); ?>;
    var locations = <?php echo json_encode($locations); ?>;
    updateSublocations(sublocations, allsublocations, locations, labelsublocation, labellocation, true);
}

</script>
</body>