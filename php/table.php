<head>
<script src="../js/TableFilters.js"></script>
</head>
<?php
 require 'PSUtils.php';
 require 'BuildTable.php';
 echo "<a href='../parsetable.php'>  Go to main page</a>";
 $status = $_REQUEST['status'];
 $PSTYPE = $_REQUEST["pstype"];
 
  $psutil = new PSUtils();
 $locations = $psutil-> getLocations();
 $statuses = $psutil-> getStatuses();
 $pstypes = $psutil-> getPSTypes();
 
 $allsublocations = array();
 $sublocationmap = array();
 foreach($locations as $location){
  $sublocationmap[$location] = array();
  $sublocations = $psutil -> getSubLocationsForLocation($location);
  for ($i=0; $i<count($sublocations);$i++){
    array_push($allsublocations,$sublocations[$i]);
    array_push($sublocationmap[$location],$sublocations[$i]);
   }
 }
 
 $data = $psutil -> getData('', '', $PSTYPE,'', $status);
BuildTable($pstypes, $locations, $allsublocations, $statuses, $data);
?>