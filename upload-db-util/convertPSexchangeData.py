import pandas as pd
readcsv = pd.read_csv('exchange-ps.csv', delimiter=';',names=['EXCHANGEDATE', 'FAULTYSN','NEWSN', 'PROBLEMDESC','PSTYPE'])
f= open("loadExchangePSdata.csv","a")
#delete if anything is already written in the file
f.truncate(0)

for i in range(1,len(readcsv)):
  date = readcsv['EXCHANGEDATE'][i]
  faultySN = readcsv['FAULTYSN'][i]
  newSN = readcsv['NEWSN'][i]
  desc =  readcsv['PROBLEMDESC'][i]
  pstype = readcsv['PSTYPE'][i]
  faultySN_ = str(faultySN)
  newSN_ = str(newSN)
  

  sql = 'CALL HISTORICPOWERSUPPLYEXCHANGE('+faultySN_ +', ' + newSN_+', ' + '\''+str(pstype)+'\', \'p5\',\'UNKNOWN\', STR_TO_DATE(\''+str(date)+' 10:00\',\'%d.%m.%Y %h:%i\'),  '+ '\''+str(desc)+'\');\n';
  f.write(sql)
f.close()