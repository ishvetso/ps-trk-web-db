import pandas as pd
readcsv = pd.read_csv('PowerSuppliesP5.csv', delimiter=';',names=['SN', 'PSTYPE','Location'])
f= open("loadData.csv","a")
#delete if anything is already written in the file
f.truncate(0)

for i in range(1,len(readcsv)):
  SN=readcsv['SN'][i]
  BARCODE = (str) ( 30280010000000+int(SN))
  PSTYPE = readcsv['PSTYPE'][i]
  sql ='CALL REGISTERHW ('+SN+', ' + BARCODE + ', ' +PSTYPE+', NULL, \'STRIPS TRACKER\', \'p5\', NULL, \'good\');\n'
  f.write(sql)

f.close()